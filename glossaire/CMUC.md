# CMU-C - Couverture maladie universelle complémentaire
!-- SPDX-License-Identifier: MPL-2.0 -->
 
 
La Couverture maladie universelle complémentaire (CMU-C) est une couverture maladie complémentaire gratuite destinée à faciliter l'accès aux soins des personnes disposant de faibles ressources et résidant en France de façon stable et régulière.   
Elle n'est pas applicable à Mayotte.   
À partir du 1er novembre 2019, elle sera remplacée par la Complémentaire santé solidaire 


# Références
 
- [Site internet de l'Assurance maladie](https://www.ameli.fr/assure/droits-demarches/difficultes-acces-droits-soins/complementaire-sante/cmu-complementaire)